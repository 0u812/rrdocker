FROM fedora:31
RUN python3 -m pip install roadrunner tellurium matplotlib numpy
COPY ./rrcli /usr/bin
ENTRYPOINT ["rrcli"]
CMD ["--help"]
