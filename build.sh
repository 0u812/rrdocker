#!/usr/bin/env zsh

# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

# exit on failure
set -e
# echo commands as they are run
set -o verbose

# get the directory of this script
ROOT_DIR="${0:A:h}"

BUILD_DIR=/tmp/build/rrdocker
cd $_

docker build --no-cache -t rrdocker https://gitlab.com/0u812/rrdocker.git
